Readme file for the SITEMAP MENU module for Drupal
---------------------------------------------------

sitemap_menu.module is a sub_module of
site_map module.
(https://drupal.org/project/site_map)

sitemap_menu.module is a basic module that
enhance the functionalities of site_map module.

With this module, if you want to remove any
menu_item from the sitemap page then you
can do it easily by clicking on the unset
checkbox on the menu edit form.

Installation:
  Installation is like with all normal drupal modules:
  extract the 'sitemap_menu' folder from the tar file
  to the modules directory from your website
  (typically sites/all/modules).
  
Dependencies:
  This module depends only on the site_map module.
  (https://drupal.org/project/site_map).

Configuration:
  The 'Unset option' will automatically appear on the
  menu edit form when the following menu is added from the
  site_map module configuration.
  
  If you select the unset option then this menu_item is
  not shown on the sitemap page and also its sub-menus
  are not shown on the sitemap page.